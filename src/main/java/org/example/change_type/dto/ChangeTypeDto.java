package org.example.change_type.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChangeTypeDto {

    private String id;
    private String currencyOrigin;
    private String currencyDestiny;
    private Double changeType;

    public ChangeTypeDto(String currencyOrigin, String currencyDestiny, Double changeType) {
        this.currencyOrigin = currencyOrigin;
        this.currencyDestiny = currencyDestiny;
        this.changeType = changeType;
    }
}
