package org.example.change_type.dto;

public enum RoleDto {
    ROLE_USER, ROLE_ADMIN
}
