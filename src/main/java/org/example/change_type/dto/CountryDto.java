package org.example.change_type.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CountryDto {

    private String id;
    private String countryName;
    private String currency;
    private String currencyName;

    public CountryDto(String countryName, String currency, String currencyName) {
        this.countryName = countryName;
        this.currency = currency;
        this.currencyName = currencyName;
    }
}
