package org.example.change_type.models.documents;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="country")
public class Country {

    @Id
    private String id;
    private String countryName;
    private String currency;
    private String currencyName;

    public Country() {
    }

    public Country(String countryName, String currency, String currencyName) {
        this.countryName = countryName;
        this.currency = currency;
        this.currencyName = currencyName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }
}
