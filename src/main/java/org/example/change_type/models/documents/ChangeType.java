package org.example.change_type.models.documents;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="change_type")
public class ChangeType {

    @Id
    private String id;
    private String currencyOrigin;
    private String currencyDestiny;
    private Double changeType;

    public ChangeType() {
    }

    public ChangeType(String currencyOrigin, String currencyDestiny, Double changeType) {
        this.currencyOrigin = currencyOrigin;
        this.currencyDestiny = currencyDestiny;
        this.changeType = changeType;
    }

    public String getCurrencyOrigin() {
        return currencyOrigin;
    }

    public void setCurrencyOrigin(String currencyOrigin) {
        this.currencyOrigin = currencyOrigin;
    }

    public String getCurrencyDestiny() {
        return currencyDestiny;
    }

    public void setCurrencyDestiny(String currencyDestiny) {
        this.currencyDestiny = currencyDestiny;
    }

    public Double getChangeType() {
        return changeType;
    }

    public void setChangeType(Double changeType) {
        this.changeType = changeType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
