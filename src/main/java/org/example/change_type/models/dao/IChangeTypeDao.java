package org.example.change_type.models.dao;

import org.example.change_type.models.documents.ChangeType;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface IChangeTypeDao extends ReactiveMongoRepository<ChangeType,String> {

    Mono<ChangeType> findByCurrencyOriginAndCurrencyDestiny(String currencyOrigin, String currencyDestiny);
}
