package org.example.change_type.models.dao;

import org.example.change_type.models.documents.Country;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICountryDao extends ReactiveMongoRepository<Country,String> {
}
