package org.example.change_type.controllers;

import org.example.change_type.dto.CountryDto;
import org.example.change_type.services.ICountryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/v1/api/country")
public class CountryController {

    public static final Logger logger = LoggerFactory.getLogger(CountryController.class);

    @Autowired
    private ICountryService iCountryService;

    @GetMapping
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Flux<CountryDto> getCountries(){
        return iCountryService.getCountries();
    }

}
