package org.example.change_type.controllers.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChangeTypeRequest {

    private String currencyOrigin;
    private String currencyDestiny;
    private Double newChangeType;

}
