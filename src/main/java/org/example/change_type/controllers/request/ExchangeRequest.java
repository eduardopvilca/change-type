package org.example.change_type.controllers.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeRequest {

    private String currencyOrigin;
    private String currencyDestiny;
    private Double amount;


}
