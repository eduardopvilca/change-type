package org.example.change_type.controllers.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeResponse {

    private String currencyOrigin;
    private String currencyDestiny;
    private Double amountCurrencyOrigin;
    private Double amountCurrencyDestiny;
    private Double changeType;

}
