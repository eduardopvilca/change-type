package org.example.change_type.controllers;

import org.example.change_type.controllers.request.ChangeTypeRequest;
import org.example.change_type.controllers.request.ExchangeRequest;
import org.example.change_type.controllers.response.ChangeTypeResponse;
import org.example.change_type.controllers.response.ExchangeResponse;
import org.example.change_type.services.IChangeTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/v1/api/changeType")
public class ChangeTypeController {

    public static final Logger logger = LoggerFactory.getLogger(ChangeTypeController.class);

    @Autowired
    private IChangeTypeService iChangeTypeService;

    @PostMapping("/exchange")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Mono<ExchangeResponse> exchange(@RequestBody ExchangeRequest exchangeRequestMono){
        return iChangeTypeService.exchange(exchangeRequestMono);
    }

    @PatchMapping()
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Mono<ChangeTypeResponse> updateChangeType(@RequestBody ChangeTypeRequest  changeTypeRequest){
        return iChangeTypeService.updateChangeType(changeTypeRequest);
    }


}
