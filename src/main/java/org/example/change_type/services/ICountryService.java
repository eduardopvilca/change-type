package org.example.change_type.services;

import org.example.change_type.dto.CountryDto;
import reactor.core.publisher.Flux;

public interface ICountryService {

    public Flux<CountryDto> getCountries();

}
