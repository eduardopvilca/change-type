package org.example.change_type.services;

import org.example.change_type.controllers.request.ChangeTypeRequest;
import org.example.change_type.controllers.request.ExchangeRequest;
import org.example.change_type.controllers.response.ChangeTypeResponse;
import org.example.change_type.controllers.response.ExchangeResponse;
import reactor.core.publisher.Mono;

public interface IChangeTypeService {

    public Mono<ExchangeResponse> exchange(ExchangeRequest exchangeRequest);
    public Mono<ChangeTypeResponse> updateChangeType(ChangeTypeRequest changeTypeDtoMono);

}
