package org.example.change_type.services;

import org.example.change_type.controllers.request.ChangeTypeRequest;
import org.example.change_type.controllers.request.ExchangeRequest;
import org.example.change_type.controllers.response.ChangeTypeResponse;
import org.example.change_type.controllers.response.ExchangeResponse;
import org.example.change_type.models.dao.IChangeTypeDao;
import org.example.change_type.models.documents.ChangeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class ChangeTypeService implements IChangeTypeService {

    public static final Logger logger = LoggerFactory.getLogger(CountryService.class);

    @Autowired
    private IChangeTypeDao iChangeTypeDao;

    @Override
    public Mono<ExchangeResponse> exchange(ExchangeRequest exchangeRequest) {

        Mono<ChangeType> changeTypeMono = Mono.just(exchangeRequest).flatMap(e->iChangeTypeDao.findByCurrencyOriginAndCurrencyDestiny(e.getCurrencyOrigin(),e.getCurrencyDestiny()));

        return changeTypeMono.map(changeType -> new ExchangeResponse(exchangeRequest.getCurrencyOrigin(),
                                                                     exchangeRequest.getCurrencyDestiny(),
                                                                     exchangeRequest.getAmount(),
                                                   exchangeRequest.getAmount()*changeType.getChangeType(),
                                                                     changeType.getChangeType()));

    }

    @Override
    public Mono<ChangeTypeResponse> updateChangeType(ChangeTypeRequest changeTypeRequest) {

        Mono<ChangeType> changeTypeMono = Mono.just(changeTypeRequest).flatMap(e->iChangeTypeDao.findByCurrencyOriginAndCurrencyDestiny(e.getCurrencyOrigin(),e.getCurrencyDestiny()));

        return changeTypeMono.doOnNext(e -> e.setChangeType(changeTypeRequest.getNewChangeType()))
                 .flatMap(iChangeTypeDao::save)
                 .map(e -> new ChangeTypeResponse(e.getCurrencyOrigin(),e.getCurrencyDestiny(),e.getChangeType()));
    }


}
