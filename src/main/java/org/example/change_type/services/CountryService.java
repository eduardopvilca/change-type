package org.example.change_type.services;

import org.example.change_type.dto.CountryDto;
import org.example.change_type.models.dao.ICountryDao;
import org.example.change_type.utils.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Primary
@Service("CountryService")
public class CountryService implements ICountryService{

    public static final Logger logger = LoggerFactory.getLogger(CountryService.class);

    @Autowired
    private ICountryDao iCountryDao;

    @Override
    public Flux<CountryDto> getCountries() {
        return iCountryDao.findAll().map(AppUtils::entityToDto);
    }
}
