package org.example.change_type.commons;

import org.example.change_type.utils.Constants;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Builder(toBuilder = true)
@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter(value = AccessLevel.PRIVATE)
@Data
public class ResponseGeneral {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String code;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer httpCode;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object response;

    public static ResponseGeneral buildSuccessResponse(Object response){
        return ResponseGeneral.builder()
                .code(Constants.SUCCESSFUL_STATUS_CODE)
                .message(Constants.SUCCESSFUL_STATUS_MESSAGE)
                .httpCode(Constants.HTTP_STATUS_OK)
                .response(response)
                .build();
    }

}
