package org.example.change_type.utils;

public class Constants {
    private Constants() {
    }

    public static final int HTTP_STATUS_OK =200;
    public static final String SUCCESSFUL_STATUS_CODE ="0";
    public static final String SUCCESSFUL_STATUS_MESSAGE ="OK";

}
