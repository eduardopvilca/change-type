package org.example.change_type.utils;

import org.example.change_type.dto.ChangeTypeDto;
import org.example.change_type.dto.CountryDto;
import org.example.change_type.models.documents.ChangeType;
import org.example.change_type.models.documents.Country;
import org.springframework.beans.BeanUtils;

public class AppUtils {

    public static ChangeTypeDto entityToDto(ChangeType changeType){
        ChangeTypeDto changeTypeDto = new ChangeTypeDto();
        BeanUtils.copyProperties(changeType,changeTypeDto);
        return  changeTypeDto;
    }

    public static CountryDto entityToDto(Country country){
        CountryDto countryDto = new CountryDto();
        BeanUtils.copyProperties(country,countryDto);
        return  countryDto;
    }

    public static ChangeType dtoToEntity(ChangeTypeDto changeTypeDto){
        ChangeType changeType = new ChangeType();
        BeanUtils.copyProperties(changeTypeDto,changeType);
        return  changeType;
    }

    public static Country dtoToEntity(CountryDto countryDto){
        Country country = new Country();
        BeanUtils.copyProperties(countryDto,country);
        return  country;
    }

}
