package org.example.change_type;

import org.example.change_type.controllers.CountryController;
import org.example.change_type.dto.CountryDto;
import org.example.change_type.services.ICountryService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebFluxTest(CountryController.class)
class CountryTests {

	@Autowired
	private WebTestClient webTestClient;

	@MockBean
	private ICountryService iCountryService;

	@Test
	public void getCountriesTest() {
		Flux<CountryDto> countryDtoFlux = Flux.just(new CountryDto("Perú","PEN","Nuevos Soles Peruanos"),
													new CountryDto("Estados Unidos","USD","Dolares Norteamericanos"));

		when(iCountryService.getCountries()).thenReturn(countryDtoFlux);

		Flux<CountryDto> responseBody = webTestClient.get().uri("/v1/api/country")
							.exchange()
							.expectStatus().isOk()
							.returnResult(CountryDto.class)
							.getResponseBody();

		StepVerifier.create(responseBody)
				    .expectSubscription()
					.expectNext(new CountryDto("Perú","PEN","Nuevos Soles Peruanos"))
					.expectNext(new CountryDto("Estados Unidos","USD","Dolares Norteamericanos"))
					.verifyComplete();

	}

}
